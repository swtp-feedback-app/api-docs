# Feedback APP API-Dokumention

## Selber bauen

Lokal kann die API-Dokumentation mithilfe des [OpenApi Generator Docker-Container](https://hub.docker.com/r/openapitools/openapi-generator-cli) gebaut werden:

```bash
docker run --rm -v `pwd`:/api-docs --workdir /api-docs openapitools/openapi-generator-cli  generate -i openapi.yaml -g html -o docs 
```

oder mithilfe der [OpenApi Generator .jar](https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/4.3.1/openapi-generator-cli-4.3.1.jar):

```bash
java -jar <Pfad zur JAR> generate -i openapi.yaml -g html -o docs
```

Die resultierende HTML Datei befindet sich in beiden fällen im `docs` Ordner.
