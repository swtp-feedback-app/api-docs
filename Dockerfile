FROM swaggerapi/swagger-ui:v3.30.0
COPY openapi.yaml /
ENV SWAGGER_JSON=/openapi.yaml BASE_URL=/swagger
